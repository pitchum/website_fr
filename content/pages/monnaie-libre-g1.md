Title: La Ğ1 : première monnaie libre de l'histoire
Breadcrumb: La monnaie libre Ğ1
BodyClass: pages-principales

# Particularités de la Ğ1

Duniter a été codé pour faire advenir une monnaie libre.

Après des années de développement, est enfin née la première monnaie libre de l'histoire de l'humanité, le 8 mars 2017.

Nous l'avons appelée la Ğ1 (prononcez "jüne").

En plus d'être une monnaie libre, la Ğ1 possèdes plusieurs caractéristiques qui lui sont propre&nbsp;:


<section id="modele-economique">
	<div>
		<h2>
			Un modèle économique solidaire
		</h2>


		<section>
			<h3>Développeurs</h3>

			<img src="{attach}/images/g1/code.svg" />

			<p>
				Les développeurs sont volontaires. Leurs contributions sont valorisées par une caisse de 
				<a href="{filename}financements.md">financement participatif</a> en monnaie libre Ğ1 (la monnaie produite par Duniter).
			</p>
		</section>

		<section>
			<h3>Nœuds calculateurs</h3>

			<img src="{attach}/images/g1/network.svg" />

			<p>
				Les nœuds calculateurs sont financés par <strong>crowdfunding</strong> également, 
				via un système appelé <a href="{filename}miner-des-blocs/remuneration-calcul-blocs.md">Remuniter</a>.
			</p>
		</section>
	</div>
</section>

<section id="expansion-reguliere">
	<div>
		<figure>

			<figcaption>
				<h2><span>Une expansion lente,</span> <span>mais régulière</span></h2>
				

				<p>
					La Toile de Confiance de la Ğ1 a été initialisée en France par 59 personnes.
				</p>

				<p>
					Malgré quelques obstacles (difficulté des nouveaux à conceptualiser une monnaie libre, 
					puis difficulté à obtenir 5 certifications), la Toile de Confiance s'étend progressivement 
					en France, en Belgique, et même au-delà.
				</p>

				<p>
					Déjà plus de 2400 personnes sont certifiées et donc membres co-producteurs de monnaie.
				</p>

				<p>
					Si l'économie en est encore à ses débuts, elle est relativement plus dynamique
					dans les régions où sont pour l'instant concentrés l'essentiel de ses utilisateurs, 
					en Occitanie et en Mayenne notamment.
				</p>
			</figcaption>
			
			<img src="{attach}/images/g1/carte-france.png" />
		</figure>
	</div>
</section>

<section id="monnaie-reelle">
	<div>
		<h2>
			Une économie bien réelle
		</h2>

		<figure id="gchange">

			<figcaption>
				
				<p>
					Parce qu'une monnaie libre rend impossible la création monétaire arbitraire, 
					elle limite considérablement les bulles financières, et favorise par là même 
					l'économie dite "réelle".
				</p>

				<p>
					C'est actuellement ce qu'on observe dans la Ğ1 : la plupart des membres échangent des autoproductions 
					(produits "faits main" ou "faits maison", services, etc.) et des biens d'occasion (livres, vêtements, etc.).
				</p>

				<p>
					Il arrive même de voir apparaître, lors de ventes aux enchères, des valeurs économiques un peu plus prestigieuses&nbsp;: 
					ordinateurs portables, guitares folk, claviers MIDI et synthétiseurs, ordinateurs collector types Commodore ou Amiga, etc.
				</p>

				<p>
					Sur ğchange, qu'on peut voir comme "le bon coin de la Ğ1", on recense déjà
					<strong>plus de 3000 petites annonces</strong>.
				</p>

				<a class="CTA" href="https://www.gchange.fr/">
					Visiter ğchange
				</a>
			</figcaption>

			<img src="{attach}/images/g1/gchange-toulouse.png" />
		</figure>

	</div>
</section>

<section id="recherche">
	<div>
		<h2>
			<span>Une expérience</span> <span>qui intéresse la recherche</span>
		</h2>
		
		<p>
			Expérience sociale grandeur nature, la Ğ1 commence à intéresser quelques chercheuses et chercheurs, qui lui consacrent des travaux de recherche :
		</p>

		<ul>
			<li>
				<a href="https://arxiv.org/abs/1911.10792">
					<time>25 nov 2019</time>

					<cite>
							Do you trade with your friends or become friends with your trading partners? A case study in the G1 cryptocurrency
					</cite>

					<span>Nicolas Gensollen, Matthieu Latapy</span>
				</a>
			</li>
			<li>
				<a href="https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3317121">
					<time>16 jan 2019</time>

					<cite>
						‘At the Very Beginning, There’s This Dream.’ The Role of Utopia in the Workings of Local and Cryptocurrencies
					</cite>

					<span>Diane-Laure Arjaliès</span>
				</a>
			</li>
		</ul>
	</div>
</section>

<section id="croissance" class="stats">
	<div>
		<h2>
			Une monnaie qui a le vent en poupe
		</h2>

		<dl class="stats has-2">
			<dt>
				<span>24+</span>
				groupes locaux
			</dt>
			
			<dd>
				<p>
					Plus de 24 groupes locaux, pour la majorité situés en France ou en Belgique.
				</p>

				<p>
					Tous animés par des bénévoles qui, à l'occasion d'apéros ou de ğmarchés expliquent la 
					<acronym title="Théorie Relative de la Monnaie">TRM</acronym> ou le fonctionnement des 
					clients de Duniter.
				</p>
			</dd>
			
			<dt>
				<span>+900</span>
				coproducteurs en 2019
			</dt>
			
			<dd>
				<p>
					Chaque année, de plus en plus d'individus font les démarches nécessaires pour 
					rejoindre la toile de confiance afin de pouvoir co-produire la Ğ1.
				</p>

				<p>
					En 2019, plus de 900 personnes ont ainsi réussi à obtenir les 5 certifications leur permettant
					de produire chaque jour le dividende universel.
				</p>
			</dd>
		</dl>
	</div>
</section>


## Aller plus loin

* [Questions fréquentes sur la Ğ1]({filename}monnaie-libre-g1/faq-g1.md)
* [Comment obtenir des Ğ1]({filename}monnaie-libre-g1/obtenir-des-g1.md)
* [Licence Ğ1]({filename}monnaie-libre-g1/licence-g1.md)
* Plus d'infos sur [monnaie-libre.fr](https://monnaie-libre.fr)

### Articles

* *[Oh les cons, ils ont doublé Hamon !](http://blog.spyou.org/wordpress-mu/2017/03/14/oh-les-cons-ils-ont-double-hamon/)* par Spyou
* *[Une monnaie libre existe !](http://nayya.org/)* par Nay4
